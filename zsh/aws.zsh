# Aliases in this file are bash and zsh compatible

# Don't change. The following determines where YADR is installed.
yadr=$HOME/.yadr

# Get operating system
platform='unknown'
unamestr=$(uname)
if [[ $unamestr == 'Linux' ]]; then
  platform='linux'
elif [[ $unamestr == 'Darwin' ]]; then
  platform='darwin'
fi

alias jaws='ssh -i ~/.ssh/aws_northrup.pem -l ubuntu'
alias gaws='ssh -i ~/.ssh/aws_gitlab.pem -l ubuntu'

