# Aliases in this file are bash and zsh compatible

# Don't change. The following determines where YADR is installed.
yadr=$HOME/.yadr

alias pssh='ssh -J deploy.gitlab.com'
alias gssh='ssh -J lb-bastion.gprd.gitlab.com'
alias sssh='ssh -J lb-bastion.gstg.gitlab.com'

export GITLAB_CHEF_REPO_DIR='/Users/jjn/Workspace/chef-repo'

